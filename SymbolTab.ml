module SymbolTab = struct
  
  let constructor =
    let tb = Hashtbl.create 0 in
      Hashtbl.add tb "SP" "0x00"; 
      Hashtbl.add tb "LCL" "0x01"; 
      Hashtbl.add tb "ARG" "0x02";
      Hashtbl.add tb "THIS" "0x03";
      Hashtbl.add tb "THAT" "0x04";
      Hashtbl.add tb "SCREEN" "0x4000";
      Hashtbl.add tb "KBD" "0x6000";
      let rec addRX t = function
        | 0 -> Hashtbl.add t ("R0") ("0x0")
        | n -> Hashtbl.add t ("R"^(string_of_int n)) ("0x0"^(String.lowercase (Printf.sprintf "%X" n))); addRX t (n-1)
      in addRX tb 15; tb


    let table = constructor

    let top = ref 16

    let pc = ref 0
  
    let addEntry symbol address=
      Hashtbl.add table symbol address      

    let contains symbol = 
      let b_str = try (Hashtbl.find table symbol) with
        | Not_found -> "_false_"
        | _ -> "_true_"
      in match b_str with "_false_" -> false | _ -> true  

    let getAddress symbol=
      Hashtbl.find table symbol

    let addVar symbol =
      Hashtbl.add table symbol ("0x0"^(String.lowercase(Printf.sprintf "%X" (!top))));
      top:= (!top)+1

    let addLabel symbol =
        Hashtbl.add table symbol ("0x0"^(String.lowercase(Printf.sprintf "%X" (!pc))))

    let incPC () =
      pc:= (!pc) +1 

    let getPC ()=
      (!pc)

(*    let debug =
      Hashtbl.iter (fun a b -> print_endline (a^b)) table
  *)          
end;; 
