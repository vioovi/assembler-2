open Base_converter
open Code
open SymbolTab

module Parser = struct

  let init file_path = 
    open_in file_path 
        (*flush stdout;
        close_in file*)

  let clean_line line_p =
    let rec aux line hide = function  
      | n when n = (String.length line) -> ""
      | n when line.[n] = '/' -> ""          
      | n when line.[n]='#'&& hide = false->  aux line true (n+1)            
      | n when line.[n]='#'->  aux line false (n+1)            
      | n when hide = true ->  aux line true (n+1)            
      | n when line.[n] = ' ' -> aux line false (n+1)  
      | n -> (Char.escaped line.[n])^(aux line false (n+1))   
    in if (line_p = "") then "" else aux line_p false 0  

  let advance file =
    try (input_line file) with
      | End_of_file -> "***EOF***"
      | _ -> (input_line file)  

  let hasMoreCommands = function
    | "***EOF***" -> false
    | _ -> true

  let commandType = function
    | "" -> "EMPTY"
    | s when s.[0] = '@' -> "A_COMMAND"
    | s when (String.contains s '=') -> "C_COMMAND"
    | s when (String.contains s ';') -> "C_COMMAND"
    | s when (String.contains s '(') -> "LABEL"
    | "***EOF***" -> "EOF"
    | s when (String.contains s '#') -> "COMMENT"
    | s when (String.contains s '/') -> "COMMENT"
    | s -> print_endline s;"ERROR_PARSING" 

  let a_command_ str =
    String.sub str 1 (String.length str -1)    

  let a_command str =
    let num = String.sub str 1 (String.length str -1) in
    let convert = if (SymbolTab.contains num) then (SymbolTab.getAddress num) else num
    in
    match convert with
      | n when n = "0" -> let nmb = Base_converter.string_of_list(Base_converter.ten_to_base(int_of_string n) 2) 
        in Base_converter.completion nmb
      | n when n.[0] = '0' && n.[1] = 'b' -> let nmb = String.sub n 2 (String.length n -2) in 
      Base_converter.completion nmb
      | n when n.[0] = '0' && n.[1] = 'x' -> let dec =
              Base_converter.base_to_ten
              (Base_converter.list_of_string(String.sub n 2 (String.length n
              -2))) 16 in
      let nmb = Base_converter.string_of_list(Base_converter.ten_to_base dec 2)
      in Base_converter.completion nmb
      | n ->  let nmb = Base_converter.string_of_list(Base_converter.ten_to_base(int_of_string n) 2) 
      in Base_converter.completion nmb
  
  let load_var str =
    let num = String.sub str 1 (String.length str -1) in
    match (num .[0]) with
      | '0' | '1'| '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' -> ()
      | _ -> if not(SymbolTab.contains num ) then  SymbolTab.addVar num
             else ()

  let load_label line = 
      let var = String.sub line 1 (String.length line -2)
      in SymbolTab.addLabel var
      (*in if not(SymbolTab.contains var) then (SymbolTab.addLabel var) 
         else () *)

  let dest line =
   if not (String.contains line '=') then "000"   
   else
     begin
       let rec find_dest line = function
         | n when line.[n] = '=' -> "" 
         | n -> (Char.escaped line.[n]) ^ (find_dest line (n+1)) 
       in Code.dest (find_dest line 0)                                     
     end

  let comp line =
    let s_column = if (String.contains line ';') then 
                  (String.index line ';')
                else (-1)  
    and equal = if (String.contains line '=') then 
                  (String.index line '=')
                else (-1) 
    in match (equal,s_column) with 
      | (-1,-1) -> "***ERROR PARSING***"
      | (-1,n) -> Code.comp (String.sub line 0 n)
      | (m,-1) -> Code.comp (String.sub line (m+1) (String.length line -m-1))
      | (m,n) -> Code.comp (String.sub line (m+1) (n-m-1))       
    
  let jump line =
   if not (String.contains line ';') then "000"   
   else
     begin
       let rec find_jump line = function
         | n when line.[n] = ';' -> "" 
         | n -> ((find_jump line (n-1)) ^ (Char.escaped line.[n])) 
       in Code.jump (find_jump line (String.length line -1))                                     
     end

end;;
